<?php
/**
 * @file
 * pab_camping_feature.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function pab_camping_feature_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'bat_type-product-field_maximum_number_of_people'.
  $field_instances['bat_type-product-field_maximum_number_of_people'] = array(
    'bundle' => 'product',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The number of people included in the price. Additional people will be charged per night on top using the price set below.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 7,
      ),
      'display' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'bat_type',
    'fences_wrapper' => 'div',
    'field_name' => 'field_maximum_number_of_people',
    'label' => 'Number of people',
    'required' => 0,
    'settings' => array(
      'max' => '',
      'min' => 0,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 37,
    ),
  );

  // Exported field_instance: 'commerce_line_item-product-field_extra_adults'.
  $field_instances['commerce_line_item-product-field_extra_adults'] = array(
    'bundle' => 'product',
    'commerce_cart_settings' => array(
      'field_access' => 1,
    ),
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 8,
      ),
    ),
    'entity_type' => 'commerce_line_item',
    'fences_wrapper' => 'div',
    'field_name' => 'field_extra_adults',
    'label' => 'Extra Adults',
    'required' => 0,
    'settings' => array(
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 6,
    ),
  );

  // Exported field_instance:
  // 'commerce_line_item-product-field_number_of_adults'.
  $field_instances['commerce_line_item-product-field_number_of_adults'] = array(
    'bundle' => 'product',
    'commerce_cart_settings' => array(
      'field_access' => 1,
    ),
    'default_value' => array(
      0 => array(
        'value' => 2,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 7,
      ),
    ),
    'entity_type' => 'commerce_line_item',
    'fences_wrapper' => 'div',
    'field_name' => 'field_number_of_adults',
    'label' => 'Number of Adults',
    'required' => 0,
    'settings' => array(
      'max' => '',
      'min' => 1,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'commerce_product-product-field_price_per_adult'.
  $field_instances['commerce_product-product-field_price_per_adult'] = array(
    'bundle' => 'product',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Price per adult, per night. Leave blank if no additional charge',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'commerce_price',
        'settings' => array(
          'calculation' => FALSE,
        ),
        'type' => 'commerce_price_formatted_amount',
        'weight' => 1,
      ),
      'line_item' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'commerce_product',
    'fences_wrapper' => 'div',
    'field_name' => 'field_price_per_adult',
    'label' => 'Price Per Additional Adult',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'commerce_price',
      'settings' => array(
        'currency_code' => 'default',
      ),
      'type' => 'commerce_price_full',
      'weight' => 3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Extra Adults');
  t('Number of Adults');
  t('Number of people');
  t('Price Per Additional Adult');
  t('Price per adult, per night. Leave blank if no additional charge');
  t('The number of people included in the price. Additional people will be charged per night on top using the price set below.');

  return $field_instances;
}
