<?php
/**
 * @file
 * pab_camping_feature.features.inc
 */

/**
 * Implements hook_commerce_custom_offline_payments().
 */
function pab_camping_feature_commerce_custom_offline_payments() {
  $items = array(
    'cash' => array(
      'id' => 'cash',
      'title' => 'Cash',
      'description' => '',
      'information' => '',
      'format' => 'plain_text',
      'status' => 1,
      'checkout' => 0,
      'terminal' => 1,
      'fieldable' => 0,
    ),
  );
  return $items;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function pab_camping_feature_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function pab_camping_feature_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function pab_camping_feature_image_default_styles() {
  $styles = array();

  // Exported image style: product_image.
  $styles['product_image'] = array(
    'label' => 'Product Image',
    'effects' => array(
      2 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 380,
          'height' => 228,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}
