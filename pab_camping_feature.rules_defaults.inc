<?php
/**
 * @file
 * pab_camping_feature.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function pab_camping_feature_default_rules_configuration() {
  $items = array();
  $items['rules_adult_pricing'] = entity_import('rules_config', '{ "rules_adult_pricing" : {
      "LABEL" : "Additional Adult Pricing",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "1",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "pab_camping", "commerce_product_reference" ],
      "ON" : { "commerce_product_calculate_sell_price" : [] },
      "IF" : [
        { "entity_is_of_bundle" : {
            "entity" : [ "commerce-line-item" ],
            "type" : "commerce_line_item",
            "bundle" : { "value" : { "product" : "product" } }
          }
        },
        { "entity_has_field" : { "entity" : [ "commerce-line-item" ], "field" : "field_total_nights" } },
        { "entity_has_field" : { "entity" : [ "commerce-line-item" ], "field" : "field_extra_adults" } }
      ],
      "DO" : [
        { "pab_camping_adult_price" : {
            "commerce_line_item" : [ "commerce_line_item" ],
            "component_name" : "base_price",
            "round_mode" : "1"
          }
        }
      ]
    }
  }');
  $items['rules_camping_price'] = entity_import('rules_config', '{ "rules_camping_price" : {
      "LABEL" : "Camping Price",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "commerce_line_item", "commerce_product_reference" ],
      "ON" : { "commerce_product_calculate_sell_price" : [] },
      "IF" : [
        { "entity_is_of_bundle" : {
            "entity" : [ "commerce-line-item" ],
            "type" : "commerce_line_item",
            "bundle" : { "value" : { "product" : "product" } }
          }
        },
        { "entity_has_field" : { "entity" : [ "commerce-line-item" ], "field" : "field_total_nights" } },
        { "NOT data_is_empty" : { "data" : [ "commerce-line-item:field-total-nights" ] } }
      ],
      "DO" : [
        { "commerce_line_item_unit_price_multiply" : {
            "commerce_line_item" : [ "commerce_line_item" ],
            "amount" : [ "commerce-line-item:field-total-nights" ],
            "component_name" : "base_price",
            "round_mode" : "1"
          }
        }
      ]
    }
  }');
  return $items;
}
